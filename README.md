# Telia Kafka Avro Export

Avro schema:

```
./schema.json
```

Avro schema export to Python files:

```
./export/
```

## Python

Kasutasin Python 3.7

## Avro Schema

Probleem "enum" andmetüübiga.

Tegin parandused vastavalt juhendile: https://stackoverflow.com/questions/50283736/avro-enum-field

## Virutalenv / venv

```sh
virtualenv -p python3.7 venv
```

```sh
source venv/bin/activate
```

## Install

https://pypi.org/project/avro-gen3/

```sh
pip install avro-gen3
```

## Run

```sh
python3.7 test.py
```