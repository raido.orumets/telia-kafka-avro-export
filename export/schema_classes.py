import json
import os.path
import decimal
import datetime
import six
from avrogen.dict_wrapper import DictWrapper
from avrogen import avrojson
from avro.schema import RecordSchema, make_avsc_object
from avro import schema as avro_schema
from typing import List, Dict, Union, Optional


def __read_file(file_name):
    with open(file_name, "r") as f:
        return f.read()
        

def __get_names_and_schema(json_str):
    names = avro_schema.Names()
    schema = make_avsc_object(json.loads(json_str), names)
    return names, schema


SCHEMA_JSON_STR = __read_file(os.path.join(os.path.dirname(__file__), "schema.avsc"))


__NAMES, SCHEMA = __get_names_and_schema(SCHEMA_JSON_STR)
__SCHEMAS: Dict[str, RecordSchema] = {}


def get_schema_type(fullname):
    return __SCHEMAS.get(fullname)
    
    
__SCHEMAS = dict((n.fullname.lstrip("."), n) for n in six.itervalues(__NAMES.names))

class clientTypeClass(object):
    # No docs available.
    
    business = "business"
    consumer = "consumer"
    other = "other"
    
    
class numberPortabilityEventClass(DictWrapper):
    # No docs available.
    
    RECORD_SCHEMA = get_schema_type("telia.ee.resman.numberPortabilityEvent")
    def __init__(self,
        voucherNumber: str,
        recipientOperatorRegistryCode: int,
        donorOperatorRegistryCode: int,
        portingNumber: str,
        customerId: str,
        customerName: str,
        portingId: str,
        serviceNumber: str,
        numberTypeEnum: Union[None, Union[str, "numberTypeClass"]]=None,
        statusCodeEnum: Union[None, Union[str, "statusCodeClass"]]=None,
        executionDate: Union[None, int]=None,
        clientTypeEnum: Union[None, Union[str, "clientTypeClass"]]=None,
        authorizedPersonId: Union[None, str]=None,
        authorizedPersonName: Union[None, str]=None,
        legalPersonId: Union[None, str]=None,
        legalPerson: Union[None, str]=None,
    ):
        super().__init__()
        
        self.voucherNumber = voucherNumber
        self.recipientOperatorRegistryCode = recipientOperatorRegistryCode
        self.donorOperatorRegistryCode = donorOperatorRegistryCode
        self.portingNumber = portingNumber
        self.numberTypeEnum = numberTypeEnum
        self.statusCodeEnum = statusCodeEnum
        self.executionDate = executionDate
        self.customerId = customerId
        self.customerName = customerName
        self.clientTypeEnum = clientTypeEnum
        self.authorizedPersonId = authorizedPersonId
        self.authorizedPersonName = authorizedPersonName
        self.legalPersonId = legalPersonId
        self.legalPerson = legalPerson
        self.portingId = portingId
        self.serviceNumber = serviceNumber
    
    @classmethod
    def construct_with_defaults(cls) -> "numberPortabilityEventClass":
        self = cls.construct({})
        self._restore_defaults()
        
        return self
    
    def _restore_defaults(self) -> None:
        self.voucherNumber = str()
        self.recipientOperatorRegistryCode = int()
        self.donorOperatorRegistryCode = int()
        self.portingNumber = str()
        self.numberTypeEnum = None
        self.statusCodeEnum = None
        self.executionDate = None
        self.customerId = str()
        self.customerName = str()
        self.clientTypeEnum = None
        self.authorizedPersonId = self.RECORD_SCHEMA.fields_dict["authorizedPersonId"].default
        self.authorizedPersonName = self.RECORD_SCHEMA.fields_dict["authorizedPersonName"].default
        self.legalPersonId = self.RECORD_SCHEMA.fields_dict["legalPersonId"].default
        self.legalPerson = self.RECORD_SCHEMA.fields_dict["legalPerson"].default
        self.portingId = str()
        self.serviceNumber = str()
    
    
    @property
    def voucherNumber(self) -> str:
        """Getter: Unique application identifier to a number portability request. CRM-s VOUCHER_NUMBER"""
        return self._inner_dict.get('voucherNumber')  # type: ignore
    
    @voucherNumber.setter
    def voucherNumber(self, value: str) -> None:
        """Setter: Unique application identifier to a number portability request. CRM-s VOUCHER_NUMBER"""
        self._inner_dict['voucherNumber'] = value
    
    
    @property
    def recipientOperatorRegistryCode(self) -> int:
        """Getter: Business registry code of new network (recipient).CRM value: VOP_REG_NUM"""
        return self._inner_dict.get('recipientOperatorRegistryCode')  # type: ignore
    
    @recipientOperatorRegistryCode.setter
    def recipientOperatorRegistryCode(self, value: int) -> None:
        """Setter: Business registry code of new network (recipient).CRM value: VOP_REG_NUM"""
        self._inner_dict['recipientOperatorRegistryCode'] = value
    
    
    @property
    def donorOperatorRegistryCode(self) -> int:
        """Getter: Business registry code of current network (donor). CRM value: DOP_REG_NUM"""
        return self._inner_dict.get('donorOperatorRegistryCode')  # type: ignore
    
    @donorOperatorRegistryCode.setter
    def donorOperatorRegistryCode(self, value: int) -> None:
        """Setter: Business registry code of current network (donor). CRM value: DOP_REG_NUM"""
        self._inner_dict['donorOperatorRegistryCode'] = value
    
    
    @property
    def portingNumber(self) -> str:
        """Getter: Number to port (+372... ?). CRM value: PORTING_NUMBER"""
        return self._inner_dict.get('portingNumber')  # type: ignore
    
    @portingNumber.setter
    def portingNumber(self, value: str) -> None:
        """Setter: Number to port (+372... ?). CRM value: PORTING_NUMBER"""
        self._inner_dict['portingNumber'] = value
    
    
    @property
    def numberTypeEnum(self) -> Union[None, Union[str, "numberTypeClass"]]:
        """Getter: Fix or mobile number. CRM value: NUMBER_TYPE"""
        return self._inner_dict.get('numberTypeEnum')  # type: ignore
    
    @numberTypeEnum.setter
    def numberTypeEnum(self, value: Union[None, Union[str, "numberTypeClass"]]) -> None:
        """Setter: Fix or mobile number. CRM value: NUMBER_TYPE"""
        self._inner_dict['numberTypeEnum'] = value
    
    
    @property
    def statusCodeEnum(self) -> Union[None, Union[str, "statusCodeClass"]]:
        """Getter: The status of the number portability request. CRM value: STATUS_CODE"""
        return self._inner_dict.get('statusCodeEnum')  # type: ignore
    
    @statusCodeEnum.setter
    def statusCodeEnum(self, value: Union[None, Union[str, "statusCodeClass"]]) -> None:
        """Setter: The status of the number portability request. CRM value: STATUS_CODE"""
        self._inner_dict['statusCodeEnum'] = value
    
    
    @property
    def executionDate(self) -> Union[None, int]:
        """Getter: The date when the number will be ported. (Input: pp.kk.aaaa hh:mi). CRM value: EXECUTION_DATE"""
        return self._inner_dict.get('executionDate')  # type: ignore
    
    @executionDate.setter
    def executionDate(self, value: Union[None, int]) -> None:
        """Setter: The date when the number will be ported. (Input: pp.kk.aaaa hh:mi). CRM value: EXECUTION_DATE"""
        self._inner_dict['executionDate'] = value
    
    
    @property
    def customerId(self) -> str:
        """Getter: Personal identification code (isikukood) or business registry code. CRM value: CUSTOMER_ID"""
        return self._inner_dict.get('customerId')  # type: ignore
    
    @customerId.setter
    def customerId(self, value: str) -> None:
        """Setter: Personal identification code (isikukood) or business registry code. CRM value: CUSTOMER_ID"""
        self._inner_dict['customerId'] = value
    
    
    @property
    def customerName(self) -> str:
        """Getter: The name of the customer. CRM value: CUSTOMER_NAME"""
        return self._inner_dict.get('customerName')  # type: ignore
    
    @customerName.setter
    def customerName(self, value: str) -> None:
        """Setter: The name of the customer. CRM value: CUSTOMER_NAME"""
        self._inner_dict['customerName'] = value
    
    
    @property
    def clientTypeEnum(self) -> Union[None, Union[str, "clientTypeClass"]]:
        """Getter: Business, consumer or other types of client. CRM value: CLIENT_TYPE"""
        return self._inner_dict.get('clientTypeEnum')  # type: ignore
    
    @clientTypeEnum.setter
    def clientTypeEnum(self, value: Union[None, Union[str, "clientTypeClass"]]) -> None:
        """Setter: Business, consumer or other types of client. CRM value: CLIENT_TYPE"""
        self._inner_dict['clientTypeEnum'] = value
    
    
    @property
    def authorizedPersonId(self) -> Union[None, str]:
        """Getter: Personal identification code (isikukood) of the authorized person. CRM value: AUTH_PERSON_ID"""
        return self._inner_dict.get('authorizedPersonId')  # type: ignore
    
    @authorizedPersonId.setter
    def authorizedPersonId(self, value: Union[None, str]) -> None:
        """Setter: Personal identification code (isikukood) of the authorized person. CRM value: AUTH_PERSON_ID"""
        self._inner_dict['authorizedPersonId'] = value
    
    
    @property
    def authorizedPersonName(self) -> Union[None, str]:
        """Getter: Name of the authorized person. CRM value: AUTHORIZED_PERSON"""
        return self._inner_dict.get('authorizedPersonName')  # type: ignore
    
    @authorizedPersonName.setter
    def authorizedPersonName(self, value: Union[None, str]) -> None:
        """Setter: Name of the authorized person. CRM value: AUTHORIZED_PERSON"""
        self._inner_dict['authorizedPersonName'] = value
    
    
    @property
    def legalPersonId(self) -> Union[None, str]:
        """Getter: Authorizer's business registry code. CRM value: LEGAL_PERSON_ID"""
        return self._inner_dict.get('legalPersonId')  # type: ignore
    
    @legalPersonId.setter
    def legalPersonId(self, value: Union[None, str]) -> None:
        """Setter: Authorizer's business registry code. CRM value: LEGAL_PERSON_ID"""
        self._inner_dict['legalPersonId'] = value
    
    
    @property
    def legalPerson(self) -> Union[None, str]:
        """Getter: Name of the authorizer. CRM value: LEGAL_PERSON"""
        return self._inner_dict.get('legalPerson')  # type: ignore
    
    @legalPerson.setter
    def legalPerson(self, value: Union[None, str]) -> None:
        """Setter: Name of the authorizer. CRM value: LEGAL_PERSON"""
        self._inner_dict['legalPerson'] = value
    
    
    @property
    def portingId(self) -> str:
        """Getter: NSPC (National Signalling Point Code). CRM value: PORTING_ID"""
        return self._inner_dict.get('portingId')  # type: ignore
    
    @portingId.setter
    def portingId(self, value: str) -> None:
        """Setter: NSPC (National Signalling Point Code). CRM value: PORTING_ID"""
        self._inner_dict['portingId'] = value
    
    
    @property
    def serviceNumber(self) -> str:
        """Getter: NSN (National Significant Number). CRM value: SERV_NUM"""
        return self._inner_dict.get('serviceNumber')  # type: ignore
    
    @serviceNumber.setter
    def serviceNumber(self, value: str) -> None:
        """Setter: NSN (National Significant Number). CRM value: SERV_NUM"""
        self._inner_dict['serviceNumber'] = value
    
    
class numberTypeClass(object):
    # No docs available.
    
    fix = "fix"
    mobile = "mobile"
    
    
class statusCodeClass(object):
    # No docs available.
    
    TODO = "TODO"
    
    
__SCHEMA_TYPES = {
    'telia.ee.resman.clientType': clientTypeClass,
    'telia.ee.resman.numberPortabilityEvent': numberPortabilityEventClass,
    'telia.ee.resman.numberType': numberTypeClass,
    'telia.ee.resman.statusCode': statusCodeClass,
    'clientType': clientTypeClass,
    'numberPortabilityEvent': numberPortabilityEventClass,
    'numberType': numberTypeClass,
    'statusCode': statusCodeClass,
}

_json_converter = avrojson.AvroJsonConverter(use_logical_types=False, schema_types=__SCHEMA_TYPES)

