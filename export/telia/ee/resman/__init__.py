from ....schema_classes import clientTypeClass
from ....schema_classes import numberPortabilityEventClass
from ....schema_classes import numberTypeClass
from ....schema_classes import statusCodeClass


clientType = clientTypeClass
numberPortabilityEvent = numberPortabilityEventClass
numberType = numberTypeClass
statusCode = statusCodeClass
