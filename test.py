from avrogen import write_schema_files

schema_json_path = './schema.json'
output_directory = './export/'

file = open(schema_json_path)
schema_json = file.read()
file.close()

write_schema_files(schema_json, output_directory)